import Vue from "vue";
import VueRouter from "vue-router";
import Home from "@/views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/login",
    name: "Login",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "admin" */ "@/views/Admin.vue"),
  },
  {
  path: "/admin",
  component: () => import(/* webpackChunkName: "admin" */ "@/views/Settings.vue"),
  children: [
    {
      path: "",
      component: () => import(/* webpackChunkName: "admin" */ "@/components/main/Main.vue"),
    },
    {
      path: "/admin/about",
      component: () => import(/* webpackChunkName: "admin" */ "@/components/about_me/About.vue"),
      children: [
        {
          path: "",
          component: () => import(/* webpackChunkName: "admin" */ "@/components/about_me/Photo.vue")
        },
        {
          path: "/admin/about/info",
          component: () => import(/* webpackChunkName: "admin" */ "@/components/about_me/Info.vue")
        }
      ]
    },
    {
      path: "/admin/projects",
      component: () => import(/* webpackChunkName: "admin" */ "@/components/projects/Projects.vue"),
    },
    {
      path: "/admin/skills",
      component: () => import(/* webpackChunkName: "admin" */ "@/components/skills/Skills.vue"),
    },
    {
      path: "/admin/feedback",
      component: () => import(/* webpackChunkName: "admin" */ "@/components/feedback/Feedback.vue"),
    },
  ]
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
