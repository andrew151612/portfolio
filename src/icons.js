import Vue from "vue";
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { faChevronLeft, faChevronRight, faQuestionCircle, faSignOutAlt, faHome, faAddressCard, faUniversity, faBriefcase, faComments, faEnvelope, faTrashAlt, faPen, faPlus, faCheck } from '@fortawesome/free-solid-svg-icons';

library.add(faChevronLeft, faChevronRight, faQuestionCircle, faSignOutAlt, faHome, faAddressCard, faUniversity, faBriefcase, faComments, faEnvelope, faTrashAlt, faPen, faPlus, faCheck);

Vue.component('fa-icon', FontAwesomeIcon)